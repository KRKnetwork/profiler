# Profiler

Basic Logger and Timer

## Installation


```bash
npm i @krknet/profiler
```

## Usage

```javascript
// Instantiate with Task Title
const profiler = new Profiler('Task Title')

// start Timer
profiler.start()
// => ⠴  Task Title

// enter named Step
profiler.step('super important Step')
// => ⠋  Task Title - super important Step

// finish Task successfully
profiler.succeed('Message')
// => ✔  Task Title - Message - 20ms

// fail Task
profiler.fail('Reason')
// => ✖  Task Title - Reason

// get elapsed Time in ms
profiler.elapsed

// Report on Timing
profiler.report()

// output straight Success
Profiler.success('Task Title')
// => ✔  Task Title

Profiler.error('Task Title')
// => ✖  Task Title

Profiler.warn('Task Title')
// => ⚠  Task Title

Profiler.info('Task Title')
// =>  ℹ  Task Title

// variable Indent
new Profiler('Task Title', 4).start() // =>    ⠴  Task Title
Profiler.success('Task Title', 2) // =>  ✔  Task Title
```
