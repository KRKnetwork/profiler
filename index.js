const restoreCursor = require('restore-cursor')

module.exports = class Profiler {
  constructor (title, prefix = 1) {
    this.title = title
    this.prefix = Profiler.getPrefix(prefix)

    this.steps = []
    this.currentStep = null
    this.stepper = null
  }

  get stream () { return process.stderr }
  static get isInteractive () { return this.stream.isTTY && process.env.TERM !== 'dumb' && !('CI' in process.env) }
  static getPrefix (indent) { return typeof indent === 'string' ? indent : ' '.repeat(indent) }

  start () {
    this.frameIndex = 0
    this.start = process.hrtime()

    // Hide Cursor
    this.stream.write('\u001B[?25l')
    restoreCursor()

    this.render()
    this.stepper = setInterval(() => this.render(), 80)

    return this
  }

  render () {
    this.clear()

		const frame = frames[this.frameIndex]
    this.frameIndex = ++this.frameIndex % frames.length
    let text = `${this.prefix}${colors.cyan}${frame}${colors.reset}  ${this.title}`
    if (this.currentStep) text += ` - ${this.currentStep}`
    this.stream.write(text)

    return this
  }

  clear () {
    this.stream.clearLine()
    this.stream.cursorTo(0)

    return this
  }

  stop () {
    if (this.stepper) {
      clearInterval(this.stepper)
      this.stepper = null
    }

    this.stream.write('\u001B[?25h') // Show Cursor

    return this
  }

  step (text = this.steps.length + 1) {
    this.steps.push({ title: text, timing: this.elapsed })
    this.currentStep = text

    return this
  }

  succeed (message) {
    this.steps.push({ title: 'succeed', timing: this.elapsed })
    this.stop().clear()

    Profiler.success(`${this.title}${message ? ` - ${message}` : ''} - ${this.elapsed}ms`, this.prefix)
    return this
  }

  fail (reason) {
    this.steps.push({ title: 'fail', timing: this.elapsed })
    this.stop().clear()

    Profiler.error(`${this.title}${(reason ? ` - ${reason}` : '')} - ${this.elapsed}ms`, this.prefix)
    return this
  }

  report () {
    this.stop().clear()

    let lastTiming = 0

    console.log(`${this.title} Timing Report`)
    for (const step of this.steps) {
      console.log(` > ${(step.timing - lastTiming).toFixed(0).padStart(4, ' ')}ms - ${step.title}`)
      lastTiming = step.timing
    }

    return this
  }

  get elapsed () {
    return Math.floor(process.hrtime(this.start)[0] * 1000 + process.hrtime(this.start)[1] / 1000000)
  }

  static success (text, prefix = 1) {
    console.log(`${Profiler.getPrefix(prefix)}${colors.green}✔${colors.reset}  ${text}`)
  }

  static error (text, prefix = 1) {
    console.log(`${Profiler.getPrefix(prefix)}${colors.red}✖${colors.reset}  ${text}`)
  }

  static warn (text, prefix = 1) {
    console.log(`${Profiler.getPrefix(prefix)}${colors.yellow}⚠${colors.reset}  ${text}`)
  }

  static info (text, prefix = 1) {
    console.log(`${Profiler.getPrefix(prefix)}${colors.blue}ℹ${colors.reset}  ${text}`)
  }
}

const frames = ['⠋', '⠙', '⠹', '⠸', '⠼', '⠴', '⠦', '⠧', '⠇', '⠏']

const colors = {
  reset: '\x1b[0m',
  cyan: '\x1b[36m',
  red: '\x1b[31m',
  green: '\x1b[32m',
  yellow: '\x1b[33m',
  blue: '\x1b[34m'
}
